# version 0.5.0-dev
# author(s):
# reviewer(s):
# textdomain: block_league

# init.lua
orange=
blue=

# info_panel.lua
Team=

# achievements.lua
two in one=
three in one=
[!] This player doesn't exist!=
@1 ACHIEVEMENTS=

# commands.lua
mod management=

# exp_manager.lua
pass points=

# player_manager.lua
Back in the game in @1=

# privs.lua
It allows to use the /bladmin command=

# arena_lib/arena_manager.lua
The game will start soon=

# arena_lib/arena_timer.lua
No one=

# debug/testkit.lua
Leave test mode=
You already are in test mode!=
You've entered test mode=
You've left test mode=

# HUD/hud_critical.lua
CRITICAL!=

# HUD/hud_info_panel.lua
TDs=
Deaths=
Points=

# modes/TD/ball.lua
Ball reset=
NICE POINT!=
ENEMY TEAM SCORED...=
Your team lost the ball!=
Enemy team lost the ball!=
Your team got the ball!=
You got the ball!=
Enemy team got the ball!=

# weapons/pixelgun.lua
Pixelgun=

# weapons/rocket_launcher.lua
Rocket Launcher=

# weapons/smg.lua
Submachine Gun=

# weapons/sword.lua
2H Sword=

# weapons/weapons.lua
has killed @1 players in a row!=
YOU'VE KILLED @1=
You've been killed by @1=
@1 HAS KILLED @2=
@1 has been killed by @2=
You've killed yourself=
